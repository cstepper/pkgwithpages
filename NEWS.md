# pgkwithpages 0.0.2 <small>2021-09-28</small>

## 💬 documentation etc

  * Add `renv.lock` for locking the current package environment.
  * Initialize CI/CD with `.gitlab-ci.yml`.
  * Adjust `.gitlab-ci.yml` for gitlab pages.


# pgkwithpages 0.0.1 <small>2021-09-28</small>

## ✨ features and improvements

  * Add function `say_hi()` plus unit test.

## 💬 documentation etc

  * Add `README.Rmd` for general landing page.
  * Setup `pkgdown`-framework.

## 🍬 miscellaneous

  * Added a `NEWS.md` file to track changes to the package.
